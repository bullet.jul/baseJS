//Чому для роботи з input не рекомендується використовувати клавіатуру?
//обытия клавиатуры иногда использовались для отслеживания ввода данных пользователем в полях формы. Это ненадёжно, потому как ввод данных не обязательно может осуществляться с помощью клавиатуры. Существуют события input и change специально для обработки ввода
//Они срабатывают в результате любого ввода, включая Копировать/Вставить мышью и распознавание речи.

'use strict';

const parentB = document.querySelector('.btn-wrapper');
const keyItem = parentB.querySelectorAll('.btn');

const changeClass = item => {
    document.querySelector('.blue-button')?.classList.remove('blue-button');
    item.classList.add('blue-button');
};

document.addEventListener('keydown', e => {
    document.querySelector('[disabled]')?.removeAttribute('disabled');
    keyItem.forEach(item => {
        if (item.textContent.toUpperCase() === e.code.replace(/^Key/, '').toUpperCase()) {
            changeClass(item);
        }
    });
});

parentB.addEventListener('click', e => {
    if (e.target.classList.contains('btn')) {
        keyItem.forEach(item => {
            item.removeAttribute('disabled');
            if (item === e.target) {
                changeClass(item);
                item.setAttribute('disabled', true);
            }
        });
    }
});
