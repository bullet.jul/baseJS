'use strict';

const quant = 100;
let arr = [];

const button = document.querySelector('button');
const input = document.createElement('input');
const wrapper = document.createElement('div');

const fillArr = (array, quant) => {
    for (let i = 1; i <= quant; i++) {
        array.push(i);
    }
};

fillArr(arr, quant);

const paintOneCircle = (circle, i) => {
    const elem = document.createElement('div');
    elem.classList.add('circle');
    elem.style.cssText = `height: ${circle}px; width: ${circle}px; 
    background-color: ${'#' + ((Math.random() * 0x1000000) | 0x1000000).toString(16).slice(1)}; 
    border-radius: 100%; display: inline-block; margin-right: 15px; margin-top: 10px;`;

    wrapper.append(elem);
};

const paintAllCircles = circle => {
    wrapper.style.marginTop = `${circle / 2}px`;
    wrapper.style.width = `${circle * 10 + 10 * 15}px`;
    document.body.append(wrapper);

    arr.forEach((item, i) => {
        paintOneCircle(circle, i);
    });
};

button.addEventListener('click', () => {
    if (!document.querySelector('input')) {
        button.innerText = 'Намалювати';
        document.body.prepend(input);
    }
    if (!!document.querySelector('input') && input.value > 0) {
        button.setAttribute('disabled', true);

        paintAllCircles(input.value);
        input.value = '';
        return;
    }
    if (!!document.querySelector('input') && input.value <= 0) {
        return;
    }
});

wrapper?.addEventListener('click', event => {
    if (event.target.classList.contains('circle')) {
        document.querySelectorAll('.circle').forEach((item, i) => {
            if (item === event.target) {
                item.remove();
            }
        });
    }
});
