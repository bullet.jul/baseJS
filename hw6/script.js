// 1 Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування- для спец символов
// например, в регулярных выражениях  позволяет использовать спец символы не в качестве спец символов, а по их написанию (как текст)
// 2 Які засоби оголошення функцій ви знаєте?
// function declaration - function funcName()
// function expression function () - без имени (ф-ция анонимная), но ее можно присвоить переменной
// 3 Що таке hoisting, як він працює для змінних та функцій? - если использовать первій способ обїявления ф-ции (function funcName()),
//то она доступна до ее обявления //
// аналогично для var, переменная доступна до ее объявления и позволяет к ней обратиться
// т.е. так будет работать:

// console. log(funcName (5,10));

// c = 25;

// console.log(c);

// function funcName(a, b) {
//     return a + b;
// }

// var c = 36;

'use strict';

const createNewUser = () => {
    const firstName = prompt("Встановіть ваше ім'я")?.trim();
    const lastName = prompt('Встановіть ваше прізвище')?.trim();
    let birthday = prompt('Встановіть вашу дату народження в форматі dd.mm.yyyy');

    while (!/^\d{2}\.\d{2}\.\d{4}$/.test(birthday)) {
        birthday = prompt('Встановіть  в форматі dd.mm.yyyy', birthday === null ? 'dd.mm.yyyy' : birthday);
    }

    let [day, month, year] = birthday.split('.');

    month = month - 1; //Отсчёт месяцев month начинается с нуля 0

    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin: function () {
            if (!this.firstName || !this.lastName) {
                return 'error';
            }
            return this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            const now = Date.now();
            const dateForBirhday = Date.parse(new Date(year, month, day));

            return Math.floor((now - dateForBirhday) / (1000 * 60 * 60 * 24 * 365));
        },
        getPassword: function () {
            if (!this.firstName || !this.lastName) {
                return 'error';
            }

            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + year;
        },
    };

    return newUser;
};

const myUser = createNewUser();

console.log(myUser);

console.log(myUser.getPassword());

console.log(myUser.getAge());
