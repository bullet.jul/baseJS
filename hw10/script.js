'use strict';

document.addEventListener('DOMContentLoaded', () => {
    const tabContent = document.querySelectorAll('.tabcontent');
    const tabContentParent = document.querySelector('.tabs-content');
    const tabItem = document.querySelectorAll('.tabs-title');
    const tabParent = document.querySelector('.tabs');

    const hideTabContent = () => {
        tabContent.forEach(item => {
            item.classList.add('hide');
            item.classList.remove('show');
        });
        tabItem.forEach(item => {
            item.classList.remove('active');
        });
    };

    const showTabContent = (i = 0) => {
        tabContent[i].classList.remove('hide');
        tabContent[i].classList.add('show');
        tabItem[i].classList.add('active');
    };

    hideTabContent();
    showTabContent();

    tabParent.addEventListener('click', event => {
        if (!!tabItem && event.target.classList.contains('tabs-title')) {
            //вариант 1
            tabItem.forEach((item, i) => {
                if (event.target === item) {
                    hideTabContent();
                    showTabContent(i);
                }
            });

            //вариант 2
            // const chek = event.target.getAttribute('data-tab');
            // const eventContent = tabContentParent.querySelector(`[data-tab = ${chek}]`);
            // const eventTab = tabParent.querySelector(`[data-tab = ${chek}]`);
            // hideTabContent();
            // eventContent.classList.remove('hide');
            // eventContent.classList.add('show');
            // eventTab.classList.add('active');
        }
    });
});
