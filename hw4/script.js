// 1 Описати своїми словами навіщо потрібні функції у програмуванні. - для исполнения какого-либо действия описанного в теле ф-ции
// 2  Описати своїми словами, навіщо у функцію передавати аргумент. - так как ф-ция переиспользуемая, позволит выполнить ф-цию для разных исходных данных, сократить количество одинакового кода
// 3 Що таке оператор return та як він працює всередині функції? - возвращает результат из ф-ции для его дальнейшего использования и завершает работу ф-ции, если он не указан, то ф-ция вернет undefined


'use strict'

const askNumber = (text) => {
    let number = prompt(`Введіть значення ${text}`);
    let inputValueNumber = +number;
    while (!inputValueNumber || typeof inputValueNumber !== 'number')   {   
        number = prompt(`Введіть ${text}`, number === null ? '' : number);
        inputValueNumber = +number;
    }
    return inputValueNumber;
};

const askMath = () => {
    let math = prompt('Введіть математичну операцію');
    while (!(/^[\+\-\*\/]$/.test(math))) {
        math = prompt('Введіть операцію');
    }
    return math;
};

const number1 = askNumber(1);
const number2= askNumber(2);
const math = askMath();


const calc = (numberFirst, numberSecond, math) => {
    console.log(number1, number2, math);
    switch (math) {
        case '+':
            console.log(numberFirst + numberSecond); 
            break;
        case '-':
            console.log(numberFirst - numberSecond);
            break;
        case '*':
            console.log(numberFirst * numberSecond);
            break;
        case '/':
            console.log(numberFirst / numberSecond);    
            break;   
        default:
            console.log('Error');
            break;
    }
};

calc(number1, number2, math);