const { isObject, isArray, isSameType } = require('helpers');

module.exports = deepMerge;

function deepMerge(obj1, obj2) {
    if ((!isObject(obj1) && !isArray(obj1)) || !isSameType(obj1, obj2)) {
        // если obj1 примитив или ф-ция или у двух сущностей НЕ одинаковый тип
        //то нужно вернуть второй obj , вернуть его с глубокой копией
        if (isArray(obj2) || isObject(obj2)) {
            return deepCopy(obj2);
        }

        return obj2;
    }

    if (isArray(obj1)) {
        // для 2х массивов / проверка на разные типы в начале
        return deepMergeArrays(obj1, obj2);
    }

    return deepMergeObjects(obj1, obj2);
}

function deepCopy(item) {
    if (!isArray(item) && !isObject(item)) {
        throw new TypeError(`deep copy was called with wrong argument ${item}`);
    }
    const result = isArray(item) ? [...item] : { ...item };

    for (let i of Object.keys(result)) {
        if (isArray(result[i]) || isObject(result[i])) {
            result[i] = deepCopy(result[i]);
        }
    }

    return result; //над примитивом просто никаких манипуляци  - возващаем как есть
}

function deepMergeArrays(arr1, arr2) {
    return deepCopy([...arr1, ...arr2]); // нужно сделать глубокую копию смердженого массива чтобы внутр структура тоже копировалась
}

function deepMergeObjects(obj1, obj2) {
    const result = deepCopy(obj1); // создаем результирующий объект как копию первого объекта

    for (let key of Object.keys(obj2)) {
        if (!result.hasOwnProperty(key)) {
            // если в результирующем объекте нет такого ключа
            if (isObject(obj2[key]) || isArray(obj2[key])) {
                // нужно предусмотреть что в объекте может быть не примитив и сделать глубокие копии для сохранения внутренней структуры
                result[key] = deepCopy(obj2[key]);
                continue;
            }
            result[key] = obj2[key]; // еслм не объект и не массив, то просто добавляем свойство (из второго объекта)
            continue;
        }
        result[key] = deepMerge(result[key], obj2[key]); // выходит, что и в объекте 1 и в объекте 2 есть одинакоый ключ, а значит их нужно объединить (смерджить)
    }

    return result;
} // если свойства одинаковые то они замещаются свойствами из второго объекта,
//если разные, то добавляются недостающие / новые свойства,
