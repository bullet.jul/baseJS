module.exports = {
    isObject,
    isArray,
    isSameType,
};

function isObject(obg) {
    return Object.prototype.toString.call(obg) === '[object Object]';
}

function isArray(arr) {
    return Array.isArray(arr);
}

function isSameType(item1, item2) {
    return Object.prototype.toString.call(item1) === Object.prototype.toString.call(item2);
}
