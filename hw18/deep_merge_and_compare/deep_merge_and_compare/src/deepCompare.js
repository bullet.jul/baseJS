const { isObject, isArray, isSameType } = require('helpers');

module.exports = function deepCompare(obj1, obj2) {
    if (obj1 === obj2) {
        return true;
    }

    if ((!isObject(obj1) && !isArray(obj1)) || (!isObject(obj2) && !isArray(obj2))) {
        return false;
    }
    //Object.keys для массива тоже нормально отработает - вернет "ключи" - порядковые номера
    if (!isSameType(obj1, obj2) || Object.keys(obj1).length !== Object.keys(obj2).length) {
        return false;
    }
    //Object.keys чтобы получить внутренние свойства объекта его прямые, не прототипные, так как через for in можно получить прототипные
    for (let key of Object.keys(obj1)) {
        if (!obj2.hasOwnProperty(key)) {
            // проверяем что если в объекте 2 среди его прямых свйоств нет такого же ключа
            return false;
        }
        if (!deepCompare(!obj1[key], obj2[key])) {
            return false;
        }
    }

    return true;
};
