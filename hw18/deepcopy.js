'use strict';

const deepCopy = baseObject => {
    //primitive type
    if (typeof baseObject !== 'object' || baseObject === null) {
        return baseObject;
    } else if (Array.isArray(baseObject)) {
        return baseObject.map(item => {
            return deepCopy(item);
        });
    } else if (typeof baseObject === 'object' && !Array.isArray(baseObject) && baseObject !== null) {
        return Object.fromEntries(
            Object.entries(baseObject).map(item => {
                return deepCopy(item);
            })
        );
    }
};

const initialObject = [
    '1 array 1',
    '1 array 2',
    { 2: 'object 1', '2field2': 'object 2', '2field3': 'object 3', array: ['3', '3 array'], '2field4': 'object 5', '2field5': 'object 5' },
    '1 arra0y 3',
    '1 array 4',
    null,
    {
        'some test object': 'some test field 1',
        'some test object 2': 'some test field 2',
        array2: ['cat', 'dog', 254, null],
        testObj: { 341: 'rfc256', 12: 'fg546' },
    },
];

let result = deepCopy(initialObject);

result[6]['array2'][3] = 'some new item for test function';

console.log('new ' + result[6]['array2'][3]);
console.log('inital ' + initialObject[6]['array2'][3]);

console.log(result);
