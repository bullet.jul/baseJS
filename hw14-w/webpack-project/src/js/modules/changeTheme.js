const changeTheme = () => {
    const switcher = document.querySelector('.switch');
    const body = document.documentElement;

    const applyTheme = selectedTheme => {
        body.className = selectedTheme;

        localStorage.setItem('theme', selectedTheme);
    };

    if (localStorage.getItem('theme') === null) {
        applyTheme('light');
    } else {
        applyTheme(localStorage.getItem('theme'));
    }

    switcher.addEventListener('click', () => {
        if (localStorage.getItem('theme') === 'light') {
            applyTheme('dark');
        } else {
            applyTheme('light');
        }
    });
};

export default changeTheme;
