const sendForm = () => {
    const initalState = {
        message: {
            loading: 'Loading',
            success: 'Form was send. Thanks',
            error: 'Something went wrong',
            passWrong: 'Password`s don`t match',
            passSimple: `The password must contain at least one: <br> Uppercase letter, lowercase letter, <br> digit and special character`,
        },
        color: {
            success: 'green',
            usual: 'grey',
            error: 'red',
        },
    };

    const setState = (message, color) => {
        statusM.innerHTML = initalState.message[message];
        statusM.style.color = initalState.color[color];
    };

    const form = document.querySelector('form'),
        inputs = document.querySelectorAll('input'),
        statusM = document.querySelector('.status'),
        passwordInput = document.querySelector('.password'),
        passwordInputConfirm = document.querySelector('.confpassword');

    const request = async (url, data) => {
        setState('loading', 'usual');
        try {
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: data,
            });

            if (!response.ok) {
                throw new Error(`Form wasn't send ${response.status}`);
            }
            setState('success', 'success');

            return await response.json();
        } catch (err) {
            setState('error', 'error');
            console.log(err);
        } finally {
            clearInputs();
            document.querySelector('.reg-checkbox-hidden').checked = false;
            setTimeout(() => {
                statusM.classList.remove('active');
            }, 2000);
        }
    };

    const clearInputs = () => {
        inputs.forEach(item => (item.value = ''));
    };

    const showError = (time = 1000, errMessage = 'passWrong') => {
        setState(errMessage, 'error');
        setTimeout(() => {
            statusM.classList.remove('active');
        }, time);
    };

    //send form - data
    form.addEventListener('submit', event => {
        event.preventDefault();

        statusM.classList.add('active');

        if (
            /[a-zа-я]/.test(passwordInput.value) &&
            /[A-ZА-Я]/.test(passwordInput.value) &&
            /[0-9]/.test(passwordInput.value) &&
            /[!@#&\?\*\$]+/.test(passwordInput.value) &&
            passwordInput.value.length >= 8
        ) {
            if (passwordInput.value !== passwordInputConfirm.value) {
                showError();
                return;
            }

            const formData = new FormData(form);
            const json = JSON.stringify(Object.fromEntries(formData.entries()));

            request('https://jsonplaceholder.typicode.com/posts', json).then(res => {
                console.log('Form send');
            });
        } else {
            showError(5000, 'passSimple');
            return;
        }
    });
};

export default sendForm;
