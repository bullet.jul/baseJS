// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
//setTimeout запустить ф-цию через указанный промежуток времени, setInterval - периодически (до остановки интервала)
// с указанным временным промежутком между итерациями
// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//"Это планирует вызов func настолько быстро, насколько это возможно.
//Но планировщик будет вызывать функцию только после завершения выполнения текущего кода."
// насколько я понимаю, это связано с тем, что таймаут это асинхронная операция и если в очереди будет что-то синхронное,
// то оно выполнится в приоритете, и так же если будет много вложенных вызовов таймаута задержка уже не будет 0мс.
// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
//потому что  не произойдет очистка сборщиком мусора и будет забивать память

'use strict';

document.addEventListener('DOMContentLoaded', () => {
    const img = document.querySelectorAll('.image-to-show');
    const btnStart = document.querySelector('.btn-start');
    const btnPause = document.querySelector('.btn-stop');
    const timerWrapper = document.querySelector('.timer');

    let startTime;
    let showInterval;
    let timer;
    let index = 0;
    const timeCycle = 3000;

    const showTime = counter => {
        const sec = String((counter / 1000).toFixed(3)).split('.');
        timerWrapper.innerText = ` ${sec[0]}s : ${sec[1]}ms`;
    };

    const updateTime = () => {
        const elapsedTime = Date.now() - startTime;
        let counter = timeCycle - elapsedTime;

        if (counter <= 0) {
            clearInterval(timer);
            timerWrapper.innerText = '00s : 000ms';
            return;
        }

        showTime(counter);
    };

    const showImg = (elem, duration = 500) => {
        elem.animate(
            [
                { opacity: '0.1', transform: 'translateX(-500px)' },
                { opacity: '0.5', transform: 'translateX(-200px)' },
                { opacity: '1', transform: 'translateX(0px)' },
            ],
            { duration }
        );
        elem.style.transform = 'translateX(0)';
    };

    const hideImg = (elem, duration = 500) => {
        elem.animate(
            [
                { opacity: '1', transform: 'translateY(0)' },
                { opacity: '0.5', transform: 'translateY(-200px)' },
                { opacity: '0.1', transform: 'translateY(-500px)' },
            ],
            { duration }
        );

        elem.style.transform = 'translateX(-500px)';
    };

    img.forEach(elem => hideImg(elem, 0));

    const manageSlides = () => {
        hideImg(img[index]);
        clearInterval(timer);
        index = (index + 1) % img.length;
        showImg(img[index]);
        startTime = Date.now();
        timer = setInterval(updateTime);
    };

    btnStart.addEventListener('click', () => {
        setTimeout(showImg(img[index]), timeCycle);
        startTime = Date.now();
        timer = setInterval(updateTime);

        btnPause.style.display = 'block';

        btnStart.setAttribute('disabled', true);
        btnPause.removeAttribute('disabled');

        showInterval = setInterval(manageSlides, timeCycle);
    });

    btnPause.addEventListener('click', () => {
        clearInterval(showInterval);
        clearInterval(timer);
        timerWrapper.innerText = '00s : 000ms';

        btnPause.setAttribute('disabled', true);
        btnStart.removeAttribute('disabled');

        btnStart.textContent = 'Відновити показ';
    });
});
