// 1 Опишіть своїми словами, що таке метод об'єкту -ф-ция, кот позволяет взаимодействовать с ключами/значениями самого объекта. Задается в самом объекте
// 2 Який тип даних може мати значення властивості об'єкта? любого
// 3 Об'єкт це посилальний тип даних. Що означає це поняття?
// он передастся не по значению, а по ссылке, например при присвоении объекта в переменную (2)
//и при изменении значения объекта в переменной 2 оно изменится по ссылке для самого объекта и при обращении к этому значению из
//переменной 1 будет также мутированное значение

'use strict';

const createNewUser = () => {
    const firstName = prompt("Встановіть ваше ім'я")?.trim();
    const lastName = prompt('Встановіть ваше прізвище')?.trim();

    const newUser = {};
    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        configurable: true,
    });
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        configurable: true,
    });
    newUser.getLogin = function () {
        if (!this.firstName || !this.lastName) {
            return 'error';
        }
        return (this.firstName[0] + this.lastName).toLowerCase();
    };

    newUser.setFirstName = function (newName) {
        Object.defineProperty(newUser, 'firstName', {
            value: newName,
        });
    };

    newUser.setLastName = function (newLastName) {
        Object.defineProperty(newUser, 'lastName', {
            value: newLastName,
        });
    };

    return newUser;
};

const myInfo = createNewUser();

console.log(myInfo);

console.log(myInfo.getLogin());
