// Опишіть своїми словами що таке Document Object Model (DOM) - объектная модель документа, возможность взаимодействия со страницей как с объектом через его методы
// Яка різниця між властивостями HTML-елементів innerHTML та innerText? - первый позволяет вставлять и получать теги, второй только текст содержимое,
// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий? querySelectorAll / querySelector либо getElementBy (дальше тип селектора например getElementById / getElementsByClassName) Мне больше нравится querySelector так как он универсальнее, например если передавать сам селектор как аргумент функции, он позволит использовать разные типы в отличие от например от getElementById, которые ожидает определенного типа/ еще я нашла. что getElementBy.. всегда возвращает псевдо-массив, даже для одного значения, получается немного сложнее обратиться к элементу чем через querySelector

'use strict';

document.addEventListener('DOMContentLoaded', () => {
    const optionsList = document.querySelector('#optionsList');
    const testParagraphByClass = document.querySelector('.testParagraph');
    //const testParagraphById = document.querySelector('#testParagraph');
    const mainHeader = document.querySelector('.main-header');
    const sectionTitle = document.querySelectorAll('.section-title');

    document.querySelectorAll('p').forEach(item => {
        item.style.backgroundColor = '#ff0000';
    });

    console.log(optionsList);

    console.log(optionsList?.parentElement);

    if (!!optionsList.childNodes) {
        optionsList.childNodes.forEach(item => {
            console.log(`${item.nodeName} _ ${item.nodeType}`);
        });
    }

    //В верстке нашла, что есть id testParagraph, а не класс, но согласно заданию "з класом testParagraph" сдеала оба варианта
    !!testParagraphByClass ? (testParagraphByClass.textContent = 'This is a paragraph') : console.log('paragraph Class wasn`t found');
    //!!testParagraphById ? testParagraphById.textContent = 'This is a paragraph': console.log('paragraph ID wasn`t found');

    mainHeader?.childNodes?.forEach(item => {
        if (item.nodeName !== '#text') {
            item.classList.add('nav-item');

            console.log(item);
        }
    });

    sectionTitle?.forEach(item => {
        item.classList.remove('section-title');
    });
});
