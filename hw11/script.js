'use strict';

document.addEventListener('DOMContentLoaded', () => {
    const form = document.querySelector('.password-form');
    const inputPass = form.querySelector('[data-pass=pass]');
    const inputPassConfirm = form.querySelector('[data-pass=confirm-pass]');
    const inputsAll = form.querySelectorAll('input');
    const parentForIcons = document.querySelectorAll('.input-wrapper');
    const iconForShowPassword = form.querySelectorAll('.fa-eye-slash');
    const iconForHidePassword = form.querySelectorAll('.fa-eye');
    const modal = document.querySelector('.modal');
    let check;

    const changeInputTypeShowPass = (inputParent = form, inputSelector = '[data-pass]') => {
        inputParent.querySelectorAll(inputSelector).forEach(item => (item.type = 'text'));
    };

    const changeInputTypeHidePass = (inputParent = form, inputSelector = '[data-pass]') => {
        inputParent.querySelectorAll(inputSelector).forEach(item => (item.type = 'password'));
    };

    const showPassword = () => {
        parentForIcons.forEach(item =>
            item.addEventListener('click', e => {
                if (!e.target.classList.contains('icon-password')) {
                    return;
                }

                const icons = item.querySelectorAll('.icon-password');
                const iconForShowPassword = item.querySelector('.fa-eye-slash');
                const iconForHidePassword = item.querySelector('.fa-eye');

                icons.forEach(elem => elem.classList.toggle('active'));

                if (iconForShowPassword.classList.contains('active')) {
                    changeInputTypeShowPass(item, '[data-pass]');
                }

                if (iconForHidePassword.classList.contains('active')) {
                    changeInputTypeHidePass(item, ' [data-pass]');
                }
            })
        );
    };

    const showError = () => {
        const div = document.createElement('div');
        div.classList.add('error');
        div.innerText = 'Потрібно ввести однакові значення';
        document.body.append(div);
    };

    const removeError = () => {
        document.querySelector('.error').remove();
    };

    const openModal = () => {
        modal.style.display = 'block';
    };

    const closeModal = () => {
        modal.style.display = 'none';
    };

    const clearInputs = () => {
        inputsAll.forEach(item => (item.value = ''));
        iconForShowPassword.forEach(item => item.classList.remove('active'));
        iconForHidePassword.forEach(item => item.classList.add('active'));
        changeInputTypeHidePass();
    };

    const comparePassword = () => {
        if (inputPass.value !== inputPassConfirm.value || !inputPass.value) {
            showError();
            setTimeout(removeError, 3000);
            return (check = false);
        } else {
            openModal();
            setTimeout(closeModal, 3000);
            return (check = true);
        }
    };

    const sendForm = () => {
        form.addEventListener('submit', e => {
            e.preventDefault();

            comparePassword();
            check && clearInputs();
        });
    };

    showPassword();
    sendForm();
});
