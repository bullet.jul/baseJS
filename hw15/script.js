//Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці? чтобы вызывать ф-цию из самой себя, тем самым зациклив
//некое действие указанное в ф-ции до настижения граничноо условия, кот указано для завершения рекурсии - вариант реализации цикла, но 
// более читаемый код получается


'use strict';


let number = Number(prompt("Введите, пожалуйста, число"));

while (isNaN(number) || !Number.isInteger(number) || number <= 0 ) {
    number = Number(prompt("Вы ввели некоректное число. Пожалуйста повторите ввод"));
}

const factorialNumber = (number) => {
    if (typeof(number) !== 'number') {
        console.log('it`s not number');
    }

    if (number <= 1) {
        return 1;
    } else {
        return number * factorialNumber(number - 1);
    }
}

console.log(factorialNumber(number));

[3, 5, 8, 13, 15, 1].forEach((item) => {
    console.log(factorialNumber(item));
});
    
    
    
