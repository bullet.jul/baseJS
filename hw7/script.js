// 1 Опишіть своїми словами як працює метод forEach. - перебирает массив поэлементно и  совершает над каждым элементом заданное в ф-ции действие // новый массив не возвращает
// Як очистити масив? - перебором через pop() удалить элементы, также есть варинт задать длину масива 0 или присвоить переменной пустой массив
// Як можна перевірити, що та чи інша змінна є масивом? например через isArray - Array.isArray([]);

'use strict';

const filterBy = (array, type) => {
    if (!Array.isArray(array)) {
        return 'it`s not Array';
    }
    return array.filter(item => typeof item !== type);
};

console.log(filterBy(['hello', 'world', 23, '238', null], 'string'));

console.log(filterBy(['hello', 'world', 23, '238', null], 'number'));

console.log(
    filterBy(
        {
            0: 'hello',
            1: 'world',
            2: '23',
        },
        'string'
    )
);
