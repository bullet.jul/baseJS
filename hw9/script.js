// Опишіть, як можна створити новий HTML тег на сторінці. createElement(тип тега) - где разместить на странице - например append / prepend
// еще можно в принципе через insertAdjacentHTML
// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// position - где именно разместить в верстке
// "beforebegin"- перед элементом
// "afterbegin" - внутри элемента, в начале
// "beforeend" - внутри элемента, в конце
// "afterend" - после элемента
// Як можна видалити елемент зі сторінки? - элемент.remove();

'use strict';

document.addEventListener('DOMContentLoaded', () => {
    let deadline = 3000;
    const timerCounter = document.querySelector('.timer');

    const viewforArray = (array, className, parent = 'body') => {
        array.map((item, i) => {
            if (Array.isArray(item)) {
                const key = Math.floor(Math.random() * 50);

                const newParent = document.createElement('ul');
                newParent.classList.add(`inner-list`);
                newParent.id = `il-${i}-${key}`;

                const elem = document.querySelector(parent).querySelectorAll(`.${className}`);
                elem[i - 1].append(newParent);

                viewforArray(item, `${className}`, `#il-${i}-${key}`);
            } else {
                document.querySelector(parent).innerHTML += `
                            <li class=${className}>${item}</li>`;
            }
        });
    };

    const deleteViewforArray = (className, parent = 'body') => {
        document
            .querySelector(parent)
            .querySelectorAll(className)
            .forEach(item => {
                item.remove();
            });
    };

    viewforArray(
        [1, '1 array', ['2', '2 array', '2 array', ['3', '3 array', ['Kharkiv', 'Kiev', ['Borispol', 'Irpin'], 'Odessa', 'Lviv', 'Dnieper']], '2 array', '2 array'], '1 array', '1 array'],
        'nav-link-item',
        '.nav-link'
    );

    let timerForDelete = setTimeout(function innerDeleteforTimer() {
        timerCounter.innerText = `00: 0${deadline / 1000}`;

        if (deadline === 0) {
            deleteViewforArray('.nav-link-item', '.nav-link');

            clearInterval(timerForDelete);
        } else {
            deadline -= 1000;
            timerForDelete = setTimeout(innerDeleteforTimer, deadline + 1000);
        }
    }, 0);
});
