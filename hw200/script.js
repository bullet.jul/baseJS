const data = [
    {
        name: 'Meredith',
        email: 'mworthy0@europa.eu',
        gender: 'Male',
        department: 'Sales',
        car: {
            name: 'Ford',
            model: 'Econoline_E250',
            vin: 'JA32U1FU9CU395336	',
        },
        locales: {
            city: 'Campo_Alegre',
            country: 'Brazil',
        },
    },
    {
        name: 'May',
        email: 'mdossettor8@tuttocitta.it',
        gender: 'Female',
        department: 'Development',
        locales: {
            city: 'Linköping',
            country: 'Sweden',
        },

        car: [
            {
                name: 'Toyota',
                model: 'Tercel',
                vin: '1FMJU1F54AE353601',
            },
            {
                name: 'Ford',
                model: 'Mustang',
                vin: '1FMJU1F54AE353601',
            },
        ],
    },
    {
        name: 'Brigid',
        email: 'bmacsweeneye@cmu.edu',
        gender: 'Female',
        department: 'Engineering',
        locales: {
            city: 'Rochester',
            country: 'United_States',
        },
        car: {
            name: 'Mazda',
            model: 'Protege',
            vin: 'WBASN0C56DD984476',
        },
    },
    {
        name: 'Antoine',
        email: 'dmessentz@google.com',
        gender: 'Male',
        department: 'Human_Resources',
        locales: {
            city: 'Ribeiro',
            country: 'Portugal',
        },
        car: {
            name: 'Dodge',
            model: 'Dakota_Club',
            vin: '1G6AV5S82F0735144',
        },
    },
];

const filterCollection = (array, whatFind, isFindAll = true, ...args) => {
    const resIndex = [];
    const innerResIndex = [];
    const result = [];
    const whereFind = [];

    let prevChecked = false;
    let checkedObj = {};

    args.forEach(item => whereFind.push(item.split('.')));

    let findingSome = (obj, index, whatFind, keyItem) => {
        for ([key, value] of Object.entries(obj)) {
            if (typeof value !== 'object' && new RegExp(value, 'i').test(whatFind) && key === keyItem) {
                console.log('primitive', keyItem, key, value);

                modifyWhatFind = modifyWhatFind.replace(new RegExp(value, 'i'), '');

                if (!isFindAll) {
                    return resIndex.push(index);
                } else {
                    innerResIndex.push(index);
                }
            } else if (Array.isArray(value) && key === keyItem) {
                console.log('array', keyItem, key, value);

                prevChecked = true;
                checkedObj = value;

                return prevChecked, checkedObj;
            } else if (typeof value === 'object' && key === keyItem) {
                console.log('object', keyItem, key, value);

                prevChecked = true;
                checkedObj = value;

                return prevChecked, checkedObj;
            } else {
                prevChecked = false;
                checkedObj = {};
            }
        }
    };

    array.forEach((element, index) => {
        modifyWhatFind = whatFind;

        whereFind.forEach(keyArray => {
            for ([i, keyItem] of keyArray.entries()) {
                if (!isFindAll && resIndex.includes(index)) {
                    continue;
                }

                if (i > 0 && !prevChecked) {
                    console.log('toNextIteraton', keyItem);
                } else if (i > 0 && prevChecked) {
                    if (Array.isArray(checkedObj)) {
                        checkedObj.forEach(subItem => {
                            findingSome(subItem, index, modifyWhatFind, keyItem);
                        });
                    } else {
                        findingSome(checkedObj, index, modifyWhatFind, keyItem);
                    }
                } else {
                    findingSome(element, index, modifyWhatFind, keyItem);
                }
            }
        });
    });

    if (!!isFindAll) {
        new Set(innerResIndex).forEach(itemCheck => {
            innerResIndex.filter(item => item === itemCheck).length === whatFind.split(' ').length && resIndex.push(itemCheck);
        });
    }

    resIndex.forEach(index => {
        result.push(array[index]);
    });

    return result;
};

console.log(filterCollection(data, 'tercel Mustang', true, 'car.model', 'locales.city', 'name'));
